<!doctype html>
<!-- [if lt IE 7]><html dir="ltr" lang="nl-be" class="no-js lt-ie7"><![endif]-->
<!-- [if lt IE 8]><html dir="ltr" lang="nl-be" class="no-js lt-ie8"><![endif]-->
<!-- [if lt IE 9]><html dir="ltr" lang="nl-be" class="no-js lt-ie9"><![endif]-->
<!-- [if lt IE 10]><html dir="ltr" lang="nl-be" class="no-js lt-ie10"><![endif]-->
<!-- [if (gt IE 9)|!(IE)]><!--><html dir="ltr" lang="nl-be" class="no-js" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>
    <!-- CHARACTER ENCODING -->
    <meta charset="UTF-8">
    <!-- MOST IMPORTANT SEO FRIENDLY TAG -->
    <title> <?php wp_title( $sep, $display, $seplocation ); ?></title>
    <!-- OTHER IMPORTANT SEO TAGS -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- HUMANS = AUTHORS TEXT FILE -->
    <link type="text/plain" rel="author" href="humans.txt">
    <!-- FAVICON AND TOUCHICONS ex: http://demosthenes.info/blog/467/Creating-MultiResolution-Favicons-For-Web-Pages-With-GIMP -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="content/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="content/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="content/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="content/icons/apple-touch-icon-57-precomposed.png">
    <link href="content/icons/favicon.ico" rel="icon" type="image/x-icon">
    <link href="content/icons/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <!-- MOBILE VIEWPORT -->
    <meta name="viewport" content="width=device-width">
    <!-- LINK NORMALIZE CSS -->
    <!-- LINK GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php bloginfo ('stylesheet_url'); ?>" type="text/css" >    
    <!-- LINK MY OWN STYLESHEET -->
    <!-- MODERNIZR: NEW HTML5 ELEMENTS + FEATURE DETECTION -->
    <script type="text/script" src="<?php bloginfo('template_url'); ?>/_scripts/libs/modernizr.costum.min.js"></script>

    <!-- Google Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Mobile Viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<![endif]-->

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo home_url(); ?>">
                <?php bloginfo('name'); ?>
            </a>
    </div>

        <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
       			'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav pull-right',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
    </div>
</nav>
</div>
</div>

